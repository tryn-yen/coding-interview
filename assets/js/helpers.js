//***********************************************
//return when there's no data available
const studentHTML404 = () => {
  const li = document.createElement('li');

  li.innerHTML = `
    <span>No Data Available</span>
  `
  filteredStudent.appendChild(li);
}

//***********************************************
//return student cards
const studentHTML = (student) => {
  //date format options
  let options = {
  dobDate: {month: 'numeric', day: 'numeric', year: 'numeric'},
  admitDate: {month: 'short', day: 'numeric', year: 'numeric'},
  anticipatedDate: {month: 'short', year: 'numeric'},
  }

  //assign date format to each data
  let dob = new Date(student.dob).toLocaleDateString('en-us', options.dobDate)
  let admitDate = new Date(student.admitDate).toLocaleDateString('en-us', options.admitDate)
  let anticipatedDate = new Date(student.anticipatedGraduationDate).toLocaleDateString('en-us', options.anticipatedDate)
 
  const li = document.createElement('li');

  li.innerHTML = `
      <span class='l-filtered__student__name'>${student.familyName}, ${student.givenName}</span>
      <span class='l-filtered__student__dob'>DOB: <span class='value'>${dob}</span></span>
      <span class='l-filtered__student__admit-date'>Admitted: <span class='l-filtered__student__admit-date__value'>${admitDate}</span></span>
      <span class='l-filtered__student__grad-date'>Anticipated Graduation: <span class='l-grad-date__value'>${anticipatedDate}</span></span>
      <span class='l-filtered__student__mentor'>Mentor: <span class='value'>${student.mentor.familyName}, ${student.mentor.givenName}</span></span>
    `
  //add completed li to student area 
  filteredStudent.appendChild(li);

}

//***********************************************
//return department list
const departmentHTML = (department) => {
  const div = document.createElement('div');
  div.classList.add('c-department');
  div.innerHTML = `
  <div class='c-department__name'>${department.name}</div>
    <div class='c-subject'>
    ${department.subjects.map(subject => {
    return `<div class='c-subject__name'>${subject}</div>`
  })}
  <button class='c-department__btn-expand subjectShowHideBtn'>Show More</button>
  </div>
  `
  departmentDiv.prepend(div)
}