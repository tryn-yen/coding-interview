const filteredStudent = document.querySelector('.l-filtered__student');
const departmentDiv = document.querySelector('.l-department');
const viewBtn = document.querySelector('.viewBtn');
const studentWrapper = document.querySelector('.c-filtered__student');

let jsonFiles = {
  studentJSON: '../json/students.json',
  departmentJSON: '../json/departments.json'
}

//***********************************************
//get student data from students.json
const fetchJSON = async (jsonFile) => {
  let response = await fetch(jsonFile);
  let data = await response.json();
  return data;
}

//***********************************************
//display all students
const displayStudents = (studentData) => {
  let students = studentData.body.studentData.students;
  filteredStudent.textContent = '';

  students.map(student => {
    studentHTML(student);
  });
}

//***********************************************
//display all departments
const displayDepartments = (deptData) => {
  const departments = deptData.departments;
  departments.map(department => {
    departmentHTML(department);
  })
}

//***********************************************
//show & hide subject filter list
const displayDiv = () => {
  let liDepartments = [...departmentDiv.children];

  if (departmentDiv.style.display !== 'none' && departmentDiv.style.display !== '') {
    departmentDiv.style.display = 'none';
    liDepartments.map(department => department.style.display = "none")
  }
  else {
    departmentDiv.style.display = 'block';
    liDepartments.map(department => department.style.display = "flex")

  }
  //Toggle between +/- when viewBtn is clicked
  viewBtn.textContent === '+' ? viewBtn.textContent = '-' : viewBtn.textContent = '+';
  hideSubjects()
}

//***********************************************
//Find total hidden results from view
const getResultLeft = (resultLeft, showMoreBtn) => {
  //Hidden results will be counted and show more button will be added
  if (resultLeft !== 0) {
    showMoreBtn.textContent = `+${resultLeft} more`;
    showMoreBtn.classList.add('hideShowBtn');
    studentWrapper.append(showMoreBtn);
  }
}

//***********************************************
//display results which fit the row
const displayResultBtn = (item, numPerRow, showMoreBtn) => {
  //hide cards which screen can't fit
  //If number of children is more than number of cards allowed per row
  //hide the rest of children
  if (item.length > numPerRow) {
    let hiddenStudents = item.slice(numPerRow);
    hiddenStudents.map(student => {
      student.classList.add('hide');
    })
    showMoreBtn.style.display = 'block';
  } //if all cards fit one line, show more button will be hidden
  else {
    showMoreBtn.style.display = 'none';
  }
}


//***********************************************
//measure the size of the card
//if cardwidth excceds screen size, then set the total card allowed per row
const checkAvaiSpace = (item, showMoreBtn) => {
  const baseOffset = item[0].offsetTop;
  const breakIndex = item.findIndex(item => item.offsetTop > baseOffset);
  const numPerRow = (breakIndex === -1 ? item.length : breakIndex);

  const resultLeft = item.length - numPerRow
  getResultLeft(resultLeft, showMoreBtn)
  displayResultBtn(item, numPerRow, showMoreBtn)
}

//***********************************************
//Toggle Show More/Show Less button
const toggleText = (e, text1, text2) => {
  let btnText = e.textContent
  let resultLeft = e.getAttribute('result-left')
  btnText.includes('Show More') ? e.innerText = text2 : e.innerText = `${resultLeft} + Show More`;
}

//***********************************************
// Toggle Overflow
const toggleOverflow = (e) => {
  e.target.parentElement.classList.remove("c-subject");
  e.target.parentElement.classList.add("c-subject--showall");
  toggleText(e.target, "Show More", "Show Less")
  e.target.classList.remove("c-department__btn-show");
}

//***********************************************
//Check an element overflow parent's element
const isOverflown = (element) => {
  return element.scrollHeight > element.clientHeight || element.scrollWidth > element.clientWidth;
}

//***********************************************
//Display Show more button only to departments where subkects overflow line 1
const displaySubjectShowBtn = () => {
  //Select all sucject Containers
  var parents = document.querySelectorAll(".c-subject");

  //Each subject container, if overflow, show show more button
  parents.forEach(parent => {
    if (isOverflown(parent)) {
      parent.lastElementChild.classList.add("c-department__btn-show");
      parent.lastElementChild.addEventListener('click', toggleOverflow);
    }
  })
}

//***********************************************
//hide student cards when it exceed 1 line & add show more or less button
const hideStudents = () => {
  //if hide/show button for student card is available, select it. Otherwise, create new button
  let showMoreBtn = document.querySelector('.hideShowBtn') || document.createElement('button');
  //sellect all filtered student cards 
  const liStudents = [...filteredStudent.children]
  console.log(liStudents)
  checkAvaiSpace(liStudents, showMoreBtn)
}


const calculateNumPerRow = () => {
  let allSubjects = document.querySelectorAll(".c-subject")

  allSubjects.forEach((subject, index) => {
    //let parentWidth = subject.clientWidth[0];
    const liSubjects = [...subject.children]
    let baseOffset = subject.offsetTop;

    const breakIndex = liSubjects.findIndex(item => item.offsetTop > baseOffset);
    const numPerRow = (breakIndex === -1 ? liSubjects.length : breakIndex);

    console.log(subject)
    //total items minus total allowed item per row, minus one due to show more/less button was included in total items
    const resultLeft = liSubjects.length - numPerRow - 1
    subject.lastElementChild.className.includes('c-subject__name') ? null : subject.lastElementChild.textContent = resultLeft + ' + Show More'   

    //save result as attribute to call when ToggleText()
    subject.lastElementChild.setAttribute('result-left', resultLeft)
  })
}
//***********************************************
//hide student cards when it exceed 1 line & add show more or less button
const hideSubjects = () => {
  //if hide/show button for student card is available, select it. Otherwise, create new button
  //  let showMoreBtn = document.querySelector('.subjectShowHideBtn') || document.createElement('button');
  calculateNumPerRow();
  displaySubjectShowBtn();
}

//***********************************************
//add query param to url
const queryParam = (chosenBtn, chosenBtnContent, chosenBtnParent) => {
  let departmentName = chosenBtn.className.includes('c-department__name')
    ? chosenBtn.textContent
    : chosenBtnParent.previousElementSibling.textContent;
  let param = chosenBtnContent === 'All Departments' || chosenBtnContent === 'All Subjects'
    ? ''
    : chosenBtn.className.includes('c-department__name')
      ? `?department=${departmentName.replace(/\s+/g, "")}`
      : `?department=${departmentName.replace(/\s+/g, "")}&subject=${chosenBtnContent.replace(/\s+/g, "")}`;
  let currentURL = window.location.protocol + "//" + window.location.host + window.location.pathname + param
  window.history.pushState({ path: currentURL }, '', currentURL);
}

//***********************************************
//display filtered students by subject
const filterStudents = async (subject) => {
  let studentJSON = await fetchJSON(jsonFiles.studentJSON);
  let students = studentJSON.body.studentData.students;

  //Use selected subject as filter condition and return all matching results
  let filteredStudents = await students.filter(student => {
    return student.subject === subject || student.department === subject
  })

  filteredStudent.textContent = '';

  //If no data available, show 'No data available'
  if (filteredStudents.length === 0) {
    studentHTML404()
  }

  //If there's data available, map through data and display students
  await filteredStudents.map((student) => {
    studentHTML(student)
  });

  //hide cards exceed one line
  hideStudents();
}


//***********************************************
//when a subject is chosen, tasks need to happen
const filterSubject = async (event) => {
  let chosenBtn = event.target;
  let chosenBtnContent = chosenBtn.textContent
  let chosenBtnParent = chosenBtn.parentElement;

  //If any subject button is chosen, except for show more btn
  if (!chosenBtn.innerText.includes('Show')) {

    //Select all elements with class name 'active'
    let activeEl = document.querySelectorAll('.active') || null;
    const liDepartments = [...departmentDiv.children]

    //remove 'active' class from previously chosen subject & department
    await activeEl.forEach(el => {
      el.classList.remove('active')
      console.log(el)
    })

    viewBtn.textContent === '+';

    //add 'active' class from newly chosen subject & department
    chosenBtn.classList.add('active');
    chosenBtn.className.includes('c-subject__name')
      ? chosenBtnParent.previousElementSibling.classList.add('active')
      : console.log('Not a subject');


    //if department is active, only show active department on page, if not, hide department
    liDepartments.map(department => {
      department.children[0].className.includes("active")
        ? department.style.display = "flex"
        : department.style.display = "none"
    })

    //move chosen filter to first in line before other subjects
    chosenBtn.textContent === "Show More" || chosenBtn.textContent === "Show Less"
      ? null
      : (chosenBtnParent.prepend(chosenBtn), console.log(chosenBtn.textContent, "Show More", "Show Less"), console.log(true))

    //add query param to url
    queryParam(chosenBtn, chosenBtnContent, chosenBtnParent)

    //filter students based on subject
    filterStudents(chosenBtnContent);

    //If all Department or All Subjects button is selected
    if (chosenBtnContent === 'All Departments' || chosenBtnContent === 'All Subjects') {
      let studentJSON = await fetchJSON(jsonFiles.studentJSON)
      displayStudents(studentJSON);
    }
  }
  //If show more/show less button is chosen
  else {
    chosenBtnContent.includes('Show More')
      ? (chosenBtnParent.classList.remove("c-subject--showall"),
        chosenBtnParent.classList.add("c-subject"),
        chosenBtn.classList.add("c-department__btn-show")
      )
      : (chosenBtnParent.classList.remove("c-subject"),
        chosenBtnParent.classList.add("c-subject--showall"),
        chosenBtn.classList.remove("c-department__btn-show")
      );
  }
}

//***********************************************
//Expand & collapse result cards
const hideShowResults = (event) => {
  let clickedBtn = event.target;
  if (clickedBtn.className === 'hideShowBtn') {
    const liStudents = [...filteredStudent.children]
    let resultLeft = 0;

    //Whenever student card class is "show", append to student container
    liStudents.map(student => {
      student.classList.toggle('show');
      filteredStudent.append(student)

      //calculate number of hidden results
      student.className.includes('hide')
        ? resultLeft++
        : resultLeft;

      //When all students are shown, change "Show More" button to "Collapse"
      student.className.includes('show')
        ? clickedBtn.textContent = 'Collapse'
        : clickedBtn.textContent = `+${resultLeft} more`;
    })
  }
}




