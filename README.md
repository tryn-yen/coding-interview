**Please find the updated video instruction HERE**
https://drive.google.com/file/d/1Aj10bXXrL0R2UReOr12y5RYbYQqEHff5/view
---

## GOALS ACHIEVED

All, including 3 stretch goals.
---

## How to run

1. Open folder in Visual Studio Code
2. Right click on index.html, choose Open with Live Server
3. Set up Live Server Extension (if Live Server option was not available in step 2)

